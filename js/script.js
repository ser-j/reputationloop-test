

app = {
    map:{},
    data:{},
    url: '',
    viewed:0,
    url_hash:'',
    url_params: [
        {name: 'noOfReviews', value:10},
        {name: 'offset', value:0}
    ],
    templates:{},
    init: function(){
        this._map();
        this._templates();
        this._get_data();


        this._bind_actions();
    },
    _templates: function () {
        var $this = this;
        $('.dot-tmpl').each(function () {
            $this.templates[this.id] = $(this).html();
            $(this).html('<i class="fa fa-refresh icon-spin"></i> Please wait....');
        });
    },

    _bind_actions: function(){
        var $this = this;

        this.map.filter_btn.click(function(){
            if($(this).attr('aria-pressed') == 'false')
            {
                $this._set_filter_value($(this).data('param'), 1);
                $this._get_data($(this));
            }
            else
            {
                $this._set_filter_value($(this).data('param'), 0);
                $this._get_data($(this));
            }
        });
        this.map.next.click(function(){
            var offset = $this._get_filter_value('offset');
            offset += $this._get_filter_value('noOfReviews');
            var total = $this.data[$this.url_hash].business_info.total_rating.total_no_of_reviews;
            if(offset < total)
            {
                var btn = $(this);
                $this._set_filter_value('offset', offset);
                $this._get_data(btn);
                $this.map.prev.show();
            }

            if( offset + $this._get_filter_value('noOfReviews') >= total)
                $this.map.next.hide();
        });
        this.map.prev.click(function(){
            var offset = $this._get_filter_value('offset');
            offset -= $this._get_filter_value('noOfReviews');
            $this.map.next.show();
            if(offset >= 0)
            {
                var btn = $(this);
                $this._set_filter_value('offset', offset);
                $this._get_data(btn);

            }
            if(!offset)
                $this.map.prev.hide();

        });
    },
    _map: function(){
        this.map = {
            table: $('#main-tbl'),
            pagination: $('#pagination'),
            info: $('#business_info'),
            reviews: $('#reviews'),
            next: $('#next'),
            prev: $('#prev'),
            filter_btn: $('.filter > button'),
            nums: $('#nums')
        };
    },
    _build_url: function(){

        if(this.url_params.length)
        {
            var url = [];
            this.url_hash = '';
            for(var j in this.url_params)
            {
                this.url_hash += this.url_params[j].name + '=' + this.url_params[j].value
                url.push(this.url_params[j].name + '=' + this.url_params[j].value);
            }
        }

        this.url =  'rla.php?'+url.join('&');

    },
    _get_data: function(btn){
        var $this = this;
        $this._build_url();
        if(typeof this.data[$this.url_hash] === 'undefined')
        {
            if(typeof btn !== 'undefined')
                btn.button('loading');
            $.ajax({
                url: $this.url,
                crossDomain: true,
                dataType: "json"
            }).done(function(data){
                if(typeof data.errors =='undefined')
                {
                    $this.data[$this.url_hash] = data;
                    $this._fill_info();
                    $this._fill_reviews();
                }
                else
                {
                    alert(data.errors.join("\n"));
                }
                if(typeof btn !== 'undefined')
                    btn.button('reset');
            }).fail(function(){
                alert('Connection error...Try later');
                if(typeof btn !== 'undefined')
                    btn.button('reset');
            });
        }
        else
        {
            $this._fill_info();
            $this._fill_reviews();
        }

    },

    _fill_info: function(){
        var tmpl = doT.template(this.templates['business_info']);

        this.map.info.html(tmpl(this.data[this.url_hash]['business_info']));

    },
    _fill_reviews: function(){
        var tmpl = doT.template(this.templates['reviews']);
        this.map.reviews.html(tmpl(this.data[this.url_hash]['reviews']));
        var num = this._get_filter_value('offset') + 10;
        var total = this.data[this.url_hash].business_info.total_rating.total_no_of_reviews;
        if(num > total)
            num = total;
        this.map.nums.html(num+' of '+total);
    },
    _set_filter_value: function(key, value){

        for(var j in this.url_params)
        {

            if(this.url_params[j].name == key)
            {

                this.url_params[j].value = value;

                return;
            }
        }
        this.url_params.push({name:key, value:value});
    }
    ,
    _get_filter_value: function(key){

        for(var j in this.url_params)
        {
            if(this.url_params[j].name == key)
            {
                return this.url_params[j].value;
            }


        }
    }
};

app.init();
