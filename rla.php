<?php

class RLA {

    protected $host = 'test.localfeedbackloop.com/api';

    protected $https = FALSE;

    protected $params = array();

    protected $memcached = array(
        'on' => TRUE,
        'data_time' => 7200,
        'host' => '127.0.0.1',
        'port' => 11211
    );

    protected $params_hash;

    protected $errors = array();

    protected $data;

    function __construct($config = array())
    {

        $defaults = array(
            'apiKey' => '61067f81f8cf7e4a1f673cd230216112',
            'threshold' => 1,
            'noOfReviews' => 10,
            'offset' => 0,
        );

        foreach($defaults as $k=>&$v)
        {
            if(!empty($config[$k]))
                $v = $config[$k];
        }

        $this->params = $defaults;
    }


    public function go()
    {
        $this->_parse_request();
        $this->_hash_params();
        $this->_memcached_init();

        if(!$this->_get_cache())
        {
            $this->_send_api_request();
            $this->_set_cache();
        }

        if(empty($this->errors))
        {

        }
        else
        {
            $this->data = array(
                'errors' => $this->errors
            );
        }

        $this->_send_response();

    }

    protected function _send_response()
    {
        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        echo $this->data;
        die();
    }

    protected function _hash_params()
    {
        $this->params_hash = md5(http_build_query($this->params));

    }

    protected function _memcached_init()
    {

        if (class_exists('Memcached') && $this->memcached['on'])
        {
            /*TODO: add logging memcached class not found*/
            $this->mc = new Memcached();
            if(!$this->mc->addserver($this->memcached['host'], $this->memcached['port']))
            {
                $this->mc = NULL;
                /*TODO: add logging fail connect*/
                return FALSE;
            }
        }
    }

    protected function _parse_request()
    {

        if(!empty($_REQUEST['offset']))
            $this->params['offset'] = (int)$_REQUEST['offset'];

        if(!empty($_REQUEST['noOfReviews']))
            $this->params['noOfReviews'] = (int)$_REQUEST['noOfReviews'];

        if(!empty($_REQUEST['yelp']))
            $this->params['yelp'] = (int)$_REQUEST['yelp'];

        if(!empty($_REQUEST['google']))
            $this->params['google'] = (int)$_REQUEST['google'];

        if(!empty($_REQUEST['internal']))
            $this->params['internal'] = (int)$_REQUEST['internal'];

    }

    protected function _get_cache()
    {
        if(!empty($this->mc))
        {
            $res = $this->mc->get($this->params_hash);
            if($res)
            {
                $this->data = $res;
                return TRUE;
            }
        }

        return FALSE;
    }

    protected function _set_cache()
    {
        if(!empty($this->mc))
        {
            return $this->mc->set($this->params_hash, $this->data, time() + $this->memcached['data_time']);
        }

        return FALSE;
    }

    protected function _build_url()
    {

        return ($this->https ? 'https://' : 'http://') .
                $this->host .
                '?'.
                http_build_query($this->params);

    }

    protected function _send_api_request()
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->_build_url());
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_ENCODING, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        $result = curl_exec($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($httpCode == 200)
        {
            $this->data = $result;
            return TRUE;
        }
        else{
            $this->errors[] = 'Response Error: '.curl_errno($ch) . ' - ' . curl_error($ch);
            return FALSE;
        }


    }



}

$do = new RLA();
$do->go();